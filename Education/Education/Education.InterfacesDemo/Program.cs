﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.InterfacesDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            IWorker[] workers = new IWorker[3]
            {
                new Manager(),
                new Worker(),
                new Robot()
            };

            foreach (var worker in workers)
            {
                worker.Work();
            }

            Console.WriteLine(" ");

            IEat[] eats = new IEat[2]
            {
                new Manager(),
                new Worker()
            };
            foreach (var eat in eats)
            {
                eat.Eat();
            }

            Console.WriteLine(" ");

            ISalary[] salaries = new ISalary[2]
            {
                new Worker(),
                new Manager()
            };

            foreach (var salary in salaries)
            {
                salary.GetSalary();
            }

            Console.ReadLine();
        }

        interface IWorker
        {
            void Work();
        }

        interface IEat
        {
            void Eat();
        }

        interface ISalary
        {
            void GetSalary();
        }

        class Manager : IWorker, IEat, ISalary
        {
            public void Work()
            {
                Console.WriteLine("Ben müdürüm çalışıyorum!");
            }

            public void Eat()
            {
                Console.WriteLine("Ben müdürüm yemek yiyiorum!");
            }

            public void GetSalary()
            {
                Console.WriteLine("Ben müdürüm maaş alıyorum!");
            }
        }

        class Worker : IWorker, IEat, ISalary
        {
            public void Work()
            {
                Console.WriteLine("Ben işçiyim çalışıyorum!");
            }

            public void Eat()
            {
                Console.WriteLine("Ben işçiyim yemek yiyiorum!");
            }

            public void GetSalary()
            {
                Console.WriteLine("Ben işçiyim maaş alıyorum!");
            }
        }

        class Robot : IWorker
        {
            public void Work()
            {
                Console.WriteLine("Ben robotum çalışıyorum!");
            }
        }
    }
}
