﻿using System;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Education.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            //Currencies currency = Currencies.Eur;
            //CheckRate(out var currencyRate, currency);
            //Console.WriteLine(currency + " " + currencyRate);
            //Console.WriteLine(currencyRate);

            //Console.WriteLine(Multiply(2, 4));
            //Console.WriteLine(Multiply(2, 4, 6));

            //Console.WriteLine(Add4(2, 3, 4, 5, 6,7));

            //string[] students = new string[3];
            //students[0] = "Semih";
            //students[1] = "Zehra";
            //students[2] = "Duru";

            //string[] students2 = new[] { "Semih", "Zehra", "Duru" };

            //string[] students3 = { "Semih", "Zehra", "Duru" };

            //foreach (var student in students2)
            //{
            //    Console.WriteLine(student);
            //}

            //Multidimension array
            //string[,] regions = new string[5, 3]

            //{
            //    {"İstanbul", "İzmit", "Balıkesir"},
            //    {"Ankara", "Konya", "Kırıkkale"},
            //    {"Antalya", "Adana", "Mersin"},
            //    {"Rize", "Trabzon", "Samsun"},
            //    {"İzmir", "Muğla", "Manisa"}
            //};

            //for (int i = 0; i <= regions.GetUpperBound(0); i++)
            //{
            //    for (int j = 0; j <= regions.GetUpperBound(1); j++)
            //    {
            //        Console.WriteLine(regions[i, j]);
            //    }

            //    Console.WriteLine("*****");
            //}

            //for
            //for (int i = 100; i >= 0; i--)
            //{
            //    Console.WriteLine(i);
            //}


            //while
            //int a = 10;
            //while (a >= 0)
            //{

            //    Console.WriteLine(a);
            //    a--;
            //}


            //int sayi = 10;
            //do
            //{
            //    Console.WriteLine(sayi);
            //    sayi--;
            //} while (sayi >= 11);

            //foreach (var student in students3)
            //{
            //    Console.WriteLine(student);
            //    Console.WriteLine(" ");
            //}


            //Console.WriteLine(IsPrimeNumber(7) ? "This is a prime number" : "This is not a prime number");



            //foreach (var item in city)
            //{
            //    Console.WriteLine(item);
            //}

            string sentence = "My name is Semih Esenturk";

            var result = sentence.Length;
            //result2 = "my Name is Zehra";
            bool result3 = sentence.EndsWith("ğ");
            bool result4 = sentence.StartsWith("My name");
            var result5 = sentence.IndexOf("name");
            var result6 = sentence.Clone();
            var result7 = sentence.LastIndexOf(" ");
            var result8 = sentence.Insert(0, "Hello, ");
            var result9 = sentence.Substring(3, 5);
            var result10 = sentence.ToLower();
            var result11 = sentence.Replace(" ", "-");
            var result12 = sentence.Remove(2, 5);


            Console.WriteLine(result12);

            Console.ReadLine();
        }



        private static bool IsPrimeNumber(int number)
        {
            bool result = true;

            for (int i = 2; i < number - 1; i++)
            {

                if (number % i == 0)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        static void CheckRate(out int currencyRate, Currencies currency)
        {
            switch (currency)
            {
                case Currencies.Usd:
                    currencyRate = 5;
                    break;
                case Currencies.Eur:
                    currencyRate = 6;
                    break;
                case Currencies.Tl:
                    currencyRate = 1;
                    break;
                default:
                    currencyRate = 0;
                    break;
            }
        }

        static int Multiply(int number1, int number2)
        {
            return number1 * number2;
        }

        static int Multiply(int number1, int number2, int number3)
        {
            return number1 * number2 * number3;
        }

        static int Add4(params int[] numbers)
        {
            return numbers.Sum();
        }

        enum Currencies
        {
            Usd,
            Eur,
            Tl
        }
    }
}
