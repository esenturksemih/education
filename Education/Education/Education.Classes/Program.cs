﻿using System;

namespace Education.Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            //CustomerManager customerManager = new CustomerManager();
            //customerManager.Add();
            //customerManager.Update();

            //Console.WriteLine(" ");

            //ProductManager productManager = new ProductManager();
            //productManager.Add();
            //productManager.Update();


            Customer customer = new Customer();
            customer.Id = 1;
            customer.FirstName = "Semih";
            customer.LastName = "Esentürk";
            customer.City = "İstanbul";

            Customer customer2 = new Customer
            {
                Id = 2,
                FirstName = "Zehra",
                LastName = "Mutlu",
                City = "Ankara"
            };


            Console.WriteLine(customer.FirstName);
            Console.WriteLine(" ");
            Console.WriteLine(customer2.FirstName);

            Console.Read();
        }
    }
}
